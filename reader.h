/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Header reader.h
	For detailed description see reader.c.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#ifndef SP_READER_H
#define SP_READER_H

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

#define MAX_ONE_LINE_LENGTH 50

/* ____________________________________________________________________________

    Structures
   ____________________________________________________________________________
*/

typedef struct {
    int color;
    double x;
    double y;
} transmitter_t;

/* ____________________________________________________________________________

    Global Module Variables
   ____________________________________________________________________________
*/

extern int freq_count;
extern int trans_radius;
extern int trans_count;
extern int count;

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

void count_file(char *file_name);
void read_file(char *file_name, transmitter_t ** p_transmitters,int index_to_frequency[]);

#endif //SP_READER_H
