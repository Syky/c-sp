/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Header error.h
	For detailed description see error.c.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#ifndef SP_ERROR_H
#define SP_ERROR_H

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

#define MISSING_ARGUMENT 1
#define OUT_OF_MEMMORY 2
#define NON_EXISTING_SOL 3
#define NO_FILE 4
#define TOO_MANY_ARGUMENT 5

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

void help();
void raise_error(int error_number);

#endif //SP_ERROR_H
