CC = gcc

.c.o:
	$(CC) -g -c $^

freq.exe: main.c error.c graph.c reader.c stack.c
	$(CC) $^ -g -o $@ -lm