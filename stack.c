/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Module stack.c
	Module which represents stack_t with his characteristic functions

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#include <stdlib.h>
#include "stack.h"
#include "error.h"

/* ____________________________________________________________________________

    stack_t *create_stack(int size)

    Creates stack with size passed as an argument.
   ____________________________________________________________________________
*/
stack_t *create_stack(int size) {
    stack_t *temp = (stack_t *) malloc(sizeof(stack_t));
    if (!temp) {
        raise_error(OUT_OF_MEMMORY);
        return NULL;
    }

    temp->size = size;
    temp->top = -1;                                     /* top -1 means that stack is empty */
    temp->array = (int *) calloc(size, sizeof(int));
    if (!temp->array) {
        free(temp);
        raise_error(OUT_OF_MEMMORY);
        return NULL;
    }

    return temp;
}

/* ____________________________________________________________________________

    void expand(stack_t *stack)

    Expands stack given in an argument. Reallocate new chunk of memory. Stack gets
    two times bigger.
   ____________________________________________________________________________
*/
void expand(stack_t *stack) {
    int* new_array = realloc(stack->array, stack->size * 2 * sizeof(int));
    if (!new_array) {
        free(stack);
        raise_error(OUT_OF_MEMMORY);
    }
    else {
    	int temp_size = stack->size;
    	
        stack->array = new_array;
        stack->size *= 2;
        int i;
        for(i = temp_size; i < stack->size; i++)
        	stack->array[i] = 0;
    }
}

/* ____________________________________________________________________________

    int is_full(stack_t *stack)

    Returns 1 (full) when top is equal to the last index.
   ____________________________________________________________________________
*/
int is_full(stack_t *stack) {
    return stack->top == stack->size - 1;
}

/* ____________________________________________________________________________

    int is_empty(stack_t *stack)

    Returns 1 (empty) when top is equal to -1.
   ____________________________________________________________________________
*/
int is_empty(stack_t *stack) {
    return stack->top == -1;
}

/* ____________________________________________________________________________

    void push(stack_t *stack, int item)

    Adds an item to the stack. If stack is full, function expands it and then
    adds item and increases top by 1.
   ____________________________________________________________________________
*/
void push(stack_t *stack, int item) {
    if (is_full(stack)) {
        expand(stack);
    }
    stack->array[++stack->top] = item;
}

/* ____________________________________________________________________________

    int pop(stack_t *stack)

    Removes an item from stack and also decreases top by 1. If the stack is
    empty, returns MIN (does not return 0, because 0 can be item.
   ____________________________________________________________________________
*/
int pop(stack_t* stack) {
    if (is_empty(stack)) {
        return MIN;
    }
    return stack->array[stack->top--];
}

/* ____________________________________________________________________________

    int is_in_full(stack_t *stack, int item)

    Returns 1 (is in stack) when given item is already in stack, otherwise
    returns 0.
   ____________________________________________________________________________
*/
int is_in_stack(stack_t *stack, int item) {
    int i;
    for (i = 0; i < stack->size; i++) {
        if (stack->array[i] == item) {
            return 1;
        }
    }
    return 0;
}

/* ____________________________________________________________________________

    void free_stack(stack_t *stack)

    Free allocated memory that stack used.
   ____________________________________________________________________________
*/
void free_stack(stack_t *stack) {
	free(stack->array);
	free(stack);
}

