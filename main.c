/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Module main.c
	Module which puts all parts together and prints result.
    Result can also be error.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph.h"
#include "error.h"

/* ____________________________________________________________________________

    void check_input(int argc)

    Checks number of arguments given as an argument argc and raise error
    if number of arguments is incorrect.
   ____________________________________________________________________________
*/
void check_input(int argc) {
    if (argc < 2) {
        raise_error(MISSING_ARGUMENT);
    }
    else if (argc > 2) {
        raise_error(TOO_MANY_ARGUMENT);
    }
}

/* ____________________________________________________________________________

    char *get_file_name(char *argv[])

    Returns file name from passed argument argv.
   ____________________________________________________________________________
*/
char *get_file_name(char *argv[]) {
    char *file_name = (char *) malloc((strlen(argv[1]) + 1) * sizeof(char));
    strncpy(file_name, argv[1], strlen(argv[1]));
    file_name[strlen(argv[1])] = '\0';
    return file_name;
}

/* ____________________________________________________________________________

    void run(int argc, char *argv[])

    Uses created functions to put everything together an prints result. After
    printing, allocated memory is freed.
   ____________________________________________________________________________
*/
void run(int argc, char *argv[]) {
    check_input(argc);
    char *file_name = get_file_name(argv);
    count_file(file_name);                  /* fills value of freq_count, trans_count, etc */

    int i,j;
    int index_to_frequency[freq_count];
    int *matrix[trans_count];               /* matrix representing graph */

    transmitter_t **p_transmitters = (transmitter_t**)calloc(trans_count, sizeof(transmitter_t*));

    for(i = 0; i < freq_count; i++) {
        index_to_frequency[i] = 0;
    }
    for (i = 0; i < trans_count; i++) {
        matrix[i] = (int *) malloc(trans_count * sizeof(int));
    }

    read_file(file_name, p_transmitters, index_to_frequency);
    init_graph(matrix);
    free(file_name);

    for(i = 0; i < trans_count; i++) {
        for (j = i+1; j < trans_count; j++) {
            if (collision(p_transmitters[i], p_transmitters[j]) == 1) {
                add_edge(matrix, i, j);
            }
        }
    }

    graph_coloring(p_transmitters, matrix);

    for(j = 0; j < trans_count; j++) {
        printf("%d %d\n", j,index_to_frequency[p_transmitters[j]->color]);
    }
    
    for (i = 0; i < trans_count; i++) {
        free(matrix[i]);
        free(p_transmitters[i]);
    }
    free(p_transmitters);
}

/* ____________________________________________________________________________

    MAIN PROGRAM
   ____________________________________________________________________________
*/
int main(int argc, char *argv[]) {
    run(argc,argv);

    return 0;
}
