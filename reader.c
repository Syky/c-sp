/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Module reader.c
	Module which is responsible for reading input file from user. Information
    gained from input are then transformed into data structures.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "error.h"
#include "reader.h"

/* ____________________________________________________________________________

    Global Module Variables
   ____________________________________________________________________________
*/

int freq_count = 0;
int trans_radius = 0;
int trans_count = 0;
int count = 0;

/* ____________________________________________________________________________

    void count_file(char *file_name)

    Reads file of given file_name and get information (or does counting) about
    frequencies, transmission radius, and transmitters.
   ____________________________________________________________________________
*/
void count_file(char *file_name) {
    char one_line[MAX_ONE_LINE_LENGTH];
    FILE *fp;

    fp =  fopen(file_name, "r");
    if (fp == NULL) {
        free(fp);
        free(file_name);
        raise_error(NO_FILE);
    }
    else {
        while (fgets(one_line, MAX_ONE_LINE_LENGTH, fp) != NULL) {       /* reads line from fp and stores it in one_line */
            count++;
            int result;
           	result = strncmp(one_line, "Transmission radius:", 20);     /* "Transmission radius:" is 20 char. long */
            if (result == 0) {
                freq_count = count - 2;     /* -2 is because one line is "Available freq.:" and one is "Transmission radius:" */
                char *s_trans_radius = fgets(one_line, MAX_ONE_LINE_LENGTH, fp); /* reads line after "Transmission radius" */
                trans_radius = atoi(s_trans_radius);
            }
        }
        trans_count = count - freq_count - 3;   /* -2 is same as above and we have to subtract one more line - "Transmitters:"*/

        fclose(fp);
    }
}

/* ____________________________________________________________________________

    void read_file(char *file_name, transmitter_t **p_transmitters, int index_to_frequency[])

    Reads file of given file_name and fills information gained from file to arrays
    p_transmitters and index_to_frequency. Array p_transmitters is filled with
    transmitters x and y and index (id) corresponds to index of p_transmitters.
    Array index_to_frequency is filled with available frequencies.
   ____________________________________________________________________________
*/
void read_file(char *file_name, transmitter_t **p_transmitters, int index_to_frequency[]) {
    char one_line[MAX_ONE_LINE_LENGTH];
    int trans = 0;          /* when string "Transmitters:" is read */
    int available = 0;      /* when string "Available:" is read */
    FILE *fp;

    fp = fopen(file_name, "r");
    if (fp == NULL) {
        raise_error(NO_FILE);
    }
    else {
        while (fgets(one_line, MAX_ONE_LINE_LENGTH, fp) != NULL) {
            int result_trans;
            int result_available;
            result_trans = strncmp(one_line, "Transmitters:",13);
            result_available = strncmp(one_line, "Available:",9);

            if (available == 1) {                                    /* This section fills array index_to_frequencies */
                char delim[] = " ";                                  /* separates every line into tokens where */
                char *splited_string = strtok(one_line, delim);      /* first token is index and second is its frequency*/
                int index_flag = 1;                                  /* index is always first on line*/
                int frequency_flag = 0;

                while(splited_string != NULL) {
                    int index;
                    int number = atoi(splited_string);

                    if (frequency_flag == 1) {
                        index_to_frequency[index] = number;

                        if (index == freq_count - 1) {
                            available = 0;                           /* breaks out from this section */
                        }
                    }
                    else if (index_flag == 1) {
                        index = number;

                        index_flag = 0;
                        frequency_flag = 1;
                    }
                    splited_string = strtok(NULL, delim);
                }

            }

            if (trans == 1) {                                       /* This section fills p_transmitters array */
                char delim[] = " ";                                 /* separates every line into tokens where */
                char *splited_string = strtok(one_line, delim);     /* first token is index, second is x and third is y*/
                int index_flag = 1;
                int x_flag = 0;
                int y_flag = 0;

                while(splited_string != NULL) {
                    char *ptr;
                    int int_number;
                    double number;
                    number = strtod(splited_string, &ptr);

                    if (y_flag == 1) {
                        p_transmitters[int_number]->y = number;

                        y_flag = 0;
                        index_flag = 0;
                    }

                    else if (x_flag == 1) {
                        p_transmitters[int_number]->x = number;

                        x_flag = 0;
                        y_flag = 1;
                    }

                    else if (index_flag == 1) {
                        int_number = number;
                        p_transmitters[int_number] = (transmitter_t *) malloc(sizeof(transmitter_t));

                        index_flag = 0;
                        x_flag = 1;
                    }
                    splited_string = strtok(NULL, delim);
                }
            }

            if (result_trans == 0) {        /* if "Transmitters:" is read, trans is set to 1 and in another */
                trans = 1;                  /* iteration it is assured that we are line under this string.*/
            }

            if (result_available == 0) {
                available = 1;
            }
        }
        fclose(fp);
    }
}

