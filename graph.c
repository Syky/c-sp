/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Module graph.c
	Module which covers operations with graph. These operations are adding
    edge, detection of collision between two transmitters and mainly graph
    coloring.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#include <math.h>
#include <stdlib.h>
#include "graph.h"
#include "stack.h"
#include "error.h"

/* ____________________________________________________________________________

    void init_graph(int *matrix[trans_count])

    Sets all values of matrix passed as an argument as zero.
   ____________________________________________________________________________
*/
void init_graph(int *matrix[trans_count]) {
    int i,j;
    for (i = 0; i < trans_count; i++) {
        for (j = 0; j < trans_count; j++) {
            matrix[i][j] = 0;
        }
    }
}

/* ____________________________________________________________________________

    void add_edge(int *matrix[trans_count], int source_index, int dest_index)

    Adds value 1 into two-dimensional array matrix passed as argument from
    source to destination.
   ____________________________________________________________________________
*/
void add_edge(int *matrix[trans_count], int source_index, int dest_index) {
    matrix[source_index][dest_index] = 1;
    matrix[dest_index][source_index] = 1;
}

/* ____________________________________________________________________________

    int collision(transmitter_t *p_t1, transmitter_t *p_t2)

    Returns 1 if two transmitters passed as arguments collide. Collide means
    that Euclidean distance between these two transmitters is less than double
    of their radius (all transmitters have same radius). Otherwise returns 0.
   ____________________________________________________________________________
*/
int collision(transmitter_t *p_t1, transmitter_t *p_t2) {
    double distance;
    double temp;

    temp = ((p_t2->x - p_t1->x) * (p_t2->x - p_t1->x)) + ((p_t2->y - p_t1->y) * (p_t2->y - p_t1->y));
    distance = sqrt(temp);
    distance = fabs(distance);

    if (distance < (2 * trans_radius)) {
        return 1;
    } else {
        return 0;
    }
}


/* ____________________________________________________________________________

    void graph_coloring(transmitter_t **p_transmitters, int *matrix[trans_count])

    Does graph coloring - if it is possible assigns to every transmitter which
    collide with another transmitter/s different color. If algorithm run out of
    available frequencies (colors) it raises an error. The matrix passed as an
    argument represents graph. Indexes of p_transmitters and matrix corresponds.
   ____________________________________________________________________________
*/
void graph_coloring(transmitter_t **p_transmitters, int *matrix[trans_count]) {
    int i, j, top;
    int frequencies[freq_count];
    
    for(i = 0; i < freq_count; i++) {
    	frequencies[i] = 0;
	}
	 
    stack_t *stack = create_stack(INITIAL_STACK_SIZE);

    for (i = 0; i < trans_count; i++) {
        p_transmitters[i]->color = -1;          /* sets every color to -1 which means no color */
    }

    for (i = 0; i < trans_count; i++) {         /* goes through all transmitters with no color */
        if (p_transmitters[i]->color == -1) {
            push(stack, i);
        }
        while (!is_empty(stack)) {
            top = pop(stack);

            for (i = 0; i < trans_count; i++) {
                if (matrix[top][i] == 1) {               /* if top has neighbour */
                    if (p_transmitters[i]->color == -1) {  /* if neighbour has no color  */
                        if (!is_in_stack(stack, i)) {
                            push(stack, i);
                        }
                    }
                    else {                              /* if neighbour has color */
                        for (j = 0; j < (sizeof(frequencies) / sizeof(int)); j++) {
                            if (p_transmitters[i]->color == j) {    /* checks what color is j e.g. (0,1,2,3) */
                                frequencies[j] = 1;                 /* fills 1 at the index of color (frequency)  */
                                break;
                            }
                        }
                    }

                }
            }

            for (i = 0; i < (sizeof(frequencies) / sizeof(int)); i++) {
                if (frequencies[i] == 0) {                   /* looks for the first free color to assign */
                    p_transmitters[top]->color = i;
                    break;
                }
            }

            for (i = 0; i < (sizeof(frequencies) / sizeof(int)); i++) {
                frequencies[i] = 0;
            }

            if (p_transmitters[top]->color == -1) {       /* algorithm can not find any color to assign */
                for (i = 0; i < trans_count; i++) {
                    free(matrix[i]);
                    free(p_transmitters[i]);
                }
                free(p_transmitters);
                free_stack(stack);
                raise_error(NON_EXISTING_SOL);
            }
        }
    }
   	free_stack(stack);
}