/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Header graph.h
	For detailed description see graph.c.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#ifndef SP_GRAPH_H
#define SP_GRAPH_H

#include "reader.h"

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

#define INITIAL_STACK_SIZE 10

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

void init_graph(int *matrix[trans_count]);
void add_edge(int *matrix[trans_count], int source_index, int dest_index);
void graph_coloring(transmitter_t ** p_transmitters, int *matrix[trans_count]);
int collision(transmitter_t *p_t1, transmitter_t *p_t2);

#endif //SP_GRAPH_H
