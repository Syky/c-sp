/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Module error.c
	Module which takes care of error handling.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/

#include <stdio.h>
#include <stdlib.h>
#include "error.h"


/* ____________________________________________________________________________

    void help()

    Utility function which prints basic information of how to use program
    properly.
   ____________________________________________________________________________
*/
void help() {
    printf("Usage:\n\tfreq.exe <filename>\n");
    printf("Description:\n\tTo run program enter freq.exe and <filename>, where <filename> represents "
           "name of text file which contains \n\tinformation about position of transmitters and available "
           "frequencies.\n");
    printf("Example:\n\t \\>freq.exe vysilace-25.txt");
}

/* ____________________________________________________________________________

    void raise_error()

    Prints error message depending on error_number argument and exits program
    with same number. In case 1 it also prints help for user.
   ____________________________________________________________________________
*/
void raise_error(int error_number) {
    switch(error_number) {
        case 1:
            printf("ERR#1: Missing argument!\n");
            help();
            exit(1);
        case 2:
            printf("ERR#2: Out of memory!\n");
            exit(2);
        case 3:
            printf("ERR#3: Non-existent solution!\n");
            exit(3);
        case 4:
            printf("ERR#4: No such file!\n");
            exit(4);
        case 5:
            printf("ERR#5: Too many arguments!\n");
            exit(5);
    }
}
