/*

    Frequencies Collision of Transmitters Solver
    Version 1.7

    Header stack.h
	For detailed description see stack.c.

    Copyright (c) Radek Müller, 2020
    Seminar work of 'Programming in C'

*/



#ifndef SP_STACK_H
#define SP_STACK_H

/* ____________________________________________________________________________

    Preprocessor-defined Symbols
   ____________________________________________________________________________
*/

#define MIN -2147483648

/* ____________________________________________________________________________

    Structures
   ____________________________________________________________________________
*/

typedef struct {
    int size;		    /* the overall size of the stack */
    int top;			/* stack_t pointer */
    int* array;
} stack_t;

/* ____________________________________________________________________________

    Function Prototypes
   ____________________________________________________________________________
*/

stack_t *create_stack(int size);
void free_stack(stack_t *s);
int is_full(stack_t *s);
int is_empty(stack_t *s);
void push(stack_t *s, int item);
int pop(stack_t *s);
int is_in_stack(stack_t* stack, int item);

#endif //SP_STACK_H
